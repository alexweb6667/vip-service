<?php


use yii\helpers\Html;
use yii\widgets\ListView;
use yii\widgets\LinkPager;
use app\models\Comments;
use app\models\Videos;

$this->title = 'Главная';

?>

<div class="full_container list_padding_container">

    <ul class="list_grid">

        <?php for($i=1;$i < 6;$i++):?>

            <li class="column_3">
                <div class="content_grid">
                    <div class="img_bg" style="background-image: url('/img/ICO/img.svg');">

                    </div>
                    <span class="title">
                        Заголовок
                    </span>
                </div>
            </li>

        <?php endfor;?>


    </ul>

</div>
