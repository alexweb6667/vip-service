<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model \app\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Регистрация';
?>

<div class="container">
    
    <div class="site-signup">

        <h1><?= $this->title ?></h1>

        <p>Регистрация временно недоступна</p>

    </div>
</div>