<?php

use yii\helpers\Html;
use app\assets\AppAsset;
use app\models\Language;


AppAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="/favicon.ico?v=2">
    <?= Html::csrfMetaTags() ?>
    <title><?= $this->title ?></title>


    <?php $this->head() ?>
</head>



<!--<body class="--><?//= empty($_COOKIE['warn']) ? 'status_load' : '';?><!--" id="--><?//= Yii::$app->controller->id . '-' . Yii::$app->controller->action->id?><!--"  >-->

<body id="<?= Yii::$app->controller->id . '-' . Yii::$app->controller->action->id?>"  >


<?php //if(empty($_COOKIE['warn'])):?>
<!---->
<!--    --><?//= Yii::$app->view->render('/layouts/_preloader');?>
<!---->
<?php //endif;?>


<div class="modal_box">


</div>

<div class="full_container padding_container header_full_container">

    <div class="container">
        <h1>Header</h1>
    </div>

</div>


    <?php $this->beginBody() ?>

        <div class="content_container" data-controller="<?= Yii::$app->controller->id;?>"  data-action="<?= Yii::$app->controller->action->id;?>" >

            <?= $content ?>

        </div>

    <?php $this->endBody() ?>

    <footer>

    </footer>


</body>

<?php $this->endPage() ?>
